/*
 *  @author: shruti.bidada
 * */

import java.io.File;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.common.xcontent.XContentType;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import config.Config;
import dto.SplitDto;
import util.ClassPathUtil;
import util.ElasticServiceUtil;
import util.FileUtil;
import util.JDBCConnectionUtil;

import static constants.Constants.CATT_OCR_METADATA;
import static constants.Constants.PDF_EXT;
import static constants.Constants.SLASH;
import static constants.Constants.TIFF_FILE_EXT_REGEX;
import static constants.Constants.UNDERSCORE;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/*
 * This <code>DownloadManager</code> is a runnable class to download the files from s3
 * */
public class DownloadAndSplitManager implements Runnable {

    private static Logger logger = LogManager.getLogger(PDFMergeManager.class);
    String s3Path;
    String localFile;
    String bucket;
    String claimNumber;
    JDBCConnectionUtil jdbcConnection;
    String destinationBucket;
    String destinationPath;
    String indexName;
    String documentType = "_doc";
    private static AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
    private static ElasticServiceUtil elasticServiceUtil = new ElasticServiceUtil();

    protected DownloadAndSplitManager(String s3Path, String localFile, String bucket, JDBCConnectionUtil jdbcConnection,
            String destinationBucket, String destinationPath, Config config) {
        this.s3Path = s3Path;
        this.localFile = localFile;
        this.bucket = bucket;
        this.claimNumber = config.getClaimNumber();
        this.jdbcConnection = jdbcConnection;
        this.destinationBucket = destinationBucket;
        this.destinationPath = destinationPath;
        this.indexName = config.getIndexName();
        elasticServiceUtil.startConnection();
    }

    @Override
    public void run() {
        if (s3client.doesObjectExist(this.bucket, this.s3Path)) {
            File localFile = new File(this.localFile);
            s3client.getObject(new GetObjectRequest(this.bucket, this.s3Path), localFile);

            BulkRequestBuilder bulkRequestBuilderReference = elasticServiceUtil.getClient().prepareBulk();

            //if file is tiff, then convert to pdf
            if (this.localFile.matches(".*" + TIFF_FILE_EXT_REGEX)) {
                FileUtil.convertTiffToPDF(this.localFile, this.localFile.replaceFirst(TIFF_FILE_EXT_REGEX, PDF_EXT));
                FileUtil.deleteDownloadedFile(this.localFile);
                //resetting local file to pdf over tiff
                this.localFile = this.localFile.replaceFirst(TIFF_FILE_EXT_REGEX, PDF_EXT);
            }
            try {
                List<SplitDto> result = FileUtil.splitPdf(this.localFile, this.localFile, PDF_EXT);
                FileUtil.deleteDownloadedFile(this.localFile);
                // Update file split status to success
                String updateSplit_status =
                        "UPDATE " + CATT_OCR_METADATA + " SET split_status='SUCCESS' WHERE claim_number='" + claimNumber
                                + "' AND s3_path='" + s3Path + "'";
                logger.info("QUERY:: " + updateSplit_status);
                jdbcConnection.executeUpdate(updateSplit_status);

                String fileName = new ArrayDeque<>(Arrays.asList(this.localFile.split("/"))).getLast();
                logger.info("Filenames" + fileName);
                String destS3PathPdf =
                        destinationPath + "pdf-split/" + claimNumber + SLASH + fileName.replace(PDF_EXT, "") + SLASH;
                String destS3PathJpeg =
                        destinationPath + "jpeg-images/" + claimNumber + SLASH + fileName.replace(PDF_EXT, "") + SLASH;

                for (SplitDto splitDto : result) {

                    //Upload Files to S3
                    String pdfPath = destS3PathPdf + last(splitDto.getPdfPageFile().split("/"));
                    logger.info("Uploading PDF pages to " + destinationBucket + SLASH + pdfPath);
                    s3client.putObject(destinationBucket, pdfPath, new File(splitDto.getPdfPageFile()));

                    String jpegPath = destS3PathPdf + last(splitDto.getJpegPageFile().split("/"));
                    logger.info("Uploading JPEG pages to " + destinationBucket + SLASH + jpegPath,
                            new File(splitDto.getJpegPageFile()));
                    s3client.putObject(destinationBucket, jpegPath, new File(splitDto.getJpegPageFile()));

                    logger.info("Index Exists " + indexName + " :: " + elasticServiceUtil.indexExists(indexName));
                    if (!elasticServiceUtil.indexExists(indexName)) {
                        logger.info("Creating index ::" + indexName);
                        elasticServiceUtil.getClient().admin().indices().prepareCreate(indexName)
                                .addMapping(documentType, ClassPathUtil.getTextfileContent("ocr_mapping.json"),
                                        XContentType.JSON).execute().actionGet();
                    }
                    logger.info("s3Path :: " + s3Path);
                    String key = s3Path.split("batch_id=")[1].split("/")[0] + UNDERSCORE + last(
                            splitDto.getPdfPageFile().replace(PDF_EXT, "").split("/"));
                    bulkRequestBuilderReference
                            .add(elasticServiceUtil.getClient().prepareIndex(indexName, documentType, key).setSource(
                                    jsonBuilder().startObject().field("pdf_bucket", destinationBucket)
                                            .field("pdf_page_path", pdfPath).field("image_bucket", destinationBucket)
                                            .field("image_path", jpegPath).endObject()));
                    try {
                        BulkResponse bulkResp = bulkRequestBuilderReference.execute().get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    //TODO: Insert result set to inventory
                    //delete local file
                    FileUtil.deleteDownloadedFile(splitDto.getPdfPageFile());
                    FileUtil.deleteDownloadedFile(splitDto.getJpegPageFile());
                }
            } catch (IOException ioExeception) {
                logger.error("IO Exception " + ioExeception.getMessage());
            }
        } else { //Failure flow - update split status to Failed
            logger.warn("S3:Object does not exists:" + this.bucket + SLASH + this.s3Path);
            String updateSplit_status =
                    "UPDATE TABLE " + CATT_OCR_METADATA + "SET split_status='FAILURE' where " + "claim_number='"
                            + claimNumber + "' AND s3_path='" + s3Path + "'";
            logger.info("QUERY:: " + updateSplit_status);
            jdbcConnection.executeUpdate(updateSplit_status);
        }
    }

    public static <T> T last(T[] array) {
        return array[array.length - 1];
    }
}