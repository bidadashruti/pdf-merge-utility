package constants;

public class Constants {

    public static final int FOUR = 4;
    public static final int TEN = 10;
    public static final int FIFTY = 50;
    public static final String PDF_EXT = ".pdf";
    public static final String TIFF_FILE_EXT_REGEX = "\\.(TIFF|tiff|TIF|tif)$";
    public static final String TIFF_PDF_FILE_EXT_REGEX = "\\.(TIFF|tiff|TIF|tif|pdf)$";
    public static final String SLASH = "/";
    public static final String UNDERSCORE="_";
    public static final String UNKNOWN = "UNKNOWN";
    public static final String TIF = ".TIF";
    public static final String TIFF = ".TIFF";
    public static final String JPEG_EXT = ".jpeg";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String CATT_OCR_METADATA = "catt_ocr_metadata";
}
