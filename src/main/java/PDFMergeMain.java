/*
 *  @author: shruti.bidada
 * */

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import config.Config;
import util.AWSSecretsMangerUtil;
import util.FileUtil;
import util.JDBCConnectionUtil;
import util.JsonUtil;
import util.MapUtil;
import util.S3Utility;

import static constants.Constants.*;

/*
 * This <code>PDFMergeMain</code> is a the main class to run the pdf merge utility
 * */
public class PDFMergeMain {

    public static final String DOWNLOAD_PATH = "/tmp/";
    private static Logger logger = LogManager.getLogger(PDFMergeMain.class);

    public static void main(String args[]) {

        if (args.length < 1) {
            throw new RuntimeException("No configuration passed");
        }
        args[0] = StringUtils.replace(args[0], "\'", "\"");
        Config config = JsonUtil.fromJson(JsonUtil.parse(args[0]), Config.class);
        logger.info("-------------------------------------------------------------------------");
        logger.info("PROCESSING PDF MERGE FOR CLAIM NUMBER::" + config.getClaimNumber());
        logger.info("Claim Number :" + config.getClaimNumber());
        logger.info("Inventory Bucket :" + config.getInventoryBucket());
        logger.info("Inventory s3InputPath :" + config.getInventoryS3Path());
        logger.info("MedicalImageBucket :" + config.getDataBucketName());
        logger.info("outputBucket :" + config.getOutputBucket());
        logger.info("s3OutputPath :" + config.getOutputS3Path());
        String localInputPath = config.getLocalInputPath() + config.getClaimNumber() + "/";
        logger.info("localInputPath :" + localInputPath);
        String localOutputPath = config.getLocalOutputPath() + config.getClaimNumber() + "/";
        logger.info("localOutputPath :" + localOutputPath);
        //EX: String topicArn = "arn:aws:sns:us-west-2:500994480637:pdf-merge-completion";
        logger.info("SNS topicArn :" + config.getTopicArn());
        logger.info("receiptHandle :" + config.getReceiptHandle());
        logger.info("ACTION :" + config.getAction());

        PDFMergeMain pdfMergeMain = new PDFMergeMain();
        if (config.getAction().equalsIgnoreCase("merge")) {
            pdfMergeMain.executePDFMerge(config, localInputPath, localOutputPath);
        } else if (config.getAction().equalsIgnoreCase("split")) {
            pdfMergeMain.executePdfSplit(config, localInputPath, localOutputPath);
        }
    }

    public void executePdfSplit(Config config, String localInputPath, String localOutputPath) {
        String region = "us-west-2";

        long startTime = System.nanoTime();
        logger.info("Starting Time:" + startTime);
        PDFMerge pdfMerge =
                new PDFMerge(localInputPath, config.getInventoryBucket(), localOutputPath, config.getOutputBucket(),
                        config.getOutputS3Path(), config.getDataBucketName());
        S3Utility.createNewDirectory(localInputPath);
        String inventoryFilename = config.getClaimNumber() + ".csv";
        Map<String, String> result = null;
        HashMap<String, String> s3FileAndBucket = new HashMap<>();

        //Establishing jdbc connectionAWSSecretKeyConnection
        //Get secret password for given secret name and region.
        String connectionDetailsJson = AWSSecretsMangerUtil.getSecret(config.getSecretName(), region);
        JsonNode connection = JsonUtil.parse(connectionDetailsJson);
        String database = "de_" + config.getTenant();
        String connectionUrl =
                "jdbc:mysql://" + connection.get("host").textValue() + ":" + connection.get("port") + SLASH;
        try {
            JSONObject obj = new JSONObject(connectionDetailsJson);
            String userName = obj.getString(USERNAME);
            String password = obj.getString(PASSWORD);

            //Connection to Mysql
            JDBCConnectionUtil jdbcConnectionToMysqlDatabase =
                    new JDBCConnectionUtil(connectionUrl, userName, password, database);

            pdfMerge.setJdbcConnection(jdbcConnectionToMysqlDatabase);
            if (config.getInventoryS3Path() != null && !config.getInventoryS3Path().isEmpty()) {
                logger.info("Starting to read the Inventory file ::" + config.getInventoryS3Path());
                S3Utility.read(inventoryFilename, region, config.getInventoryBucket(), config.getInventoryS3Path(),
                        DOWNLOAD_PATH);
                CSVReader s3_inventory = FileUtil.getCSVReader(DOWNLOAD_PATH + inventoryFilename);
                logger.info("Reading file completed ::" + inventoryFilename);

                logger.info("Parsing the file ::" + inventoryFilename);
                String[] header = new String[]{"claim_number", "bucket", "s3_path"};
                String[] line;
                while ((line = s3_inventory.readNext()) != null) {
                    result = MapUtil.buildKeyValueMap(header, line);
                    s3FileAndBucket.put(result.get("s3_path"), result.get("bucket"));
                }
            } else {
                s3FileAndBucket.put(config.getS3Key(), config.getDataBucketName());
            }
            pdfMerge.downloadFilesFromList(s3FileAndBucket, config);
        } catch (JSONException je) {
            logger.error("JSONException " + je.getMessage());
        } catch (Exception e) {
            logger.error("Encountered Exception " + e.getMessage());
        } finally {
            AmazonSNS snsClient = AmazonSNSClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
            final SNSMessageAttributes message = new SNSMessageAttributes("message");
            message.addAttribute("receiptHandle", config.getReceiptHandle());
            message.addAttribute("claimNumber", config.getClaimNumber());
            message.publish(snsClient, config.getTopicArn());
            logger.info("-------------------------------------------------------------------------");
        }
    }

    public void executePDFMerge(Config config, String localInputPath, String localOutputPath) {
        Boolean successStatus = false;
        String region = "us-west-2";

        long startTime = System.nanoTime();
        logger.info("Starting Time:" + startTime);
        PDFMerge pdfMerge =
                new PDFMerge(localInputPath, config.getInventoryBucket(), localOutputPath, config.getOutputBucket(),
                        config.getOutputS3Path(), config.getDataBucketName());
        S3Utility.createNewDirectory(localInputPath);
        String inventoryFilename = config.getClaimNumber() + ".csv";

        Map<String, String> result = null;
        LinkedHashMap<String, String> s3FileAndDate = new LinkedHashMap<>();
        try {
            // Read the s3 inventory file and parse it
            logger.info("Starting to read the Inventory file ::" + config.getInventoryS3Path());
            S3Utility.read(inventoryFilename, region, config.getInventoryBucket(), config.getInventoryS3Path(),
                    DOWNLOAD_PATH);
            CSVReader s3_inventory = FileUtil.getCSVReader(DOWNLOAD_PATH + inventoryFilename);
            logger.info("Reading file completed ::" + inventoryFilename);

            logger.info("Parsing the file ::" + inventoryFilename);
            String[] header = new String[]{"claim_number", "bucket", "s3_path", "document_date"};
            String[] line;
            while ((line = s3_inventory.readNext()) != null) {
                result = MapUtil.buildKeyValueMap(header, line);
                s3FileAndDate.put(result.get("s3_path"), result.get("document_date"));
            }
            //Downloading the files
            pdfMerge.setSorted(true);
            pdfMerge.downloadFiles(s3FileAndDate, config.getClaimNumber());

            // delete the inventory
            FileUtil.deleteDownloadedFile(DOWNLOAD_PATH + inventoryFilename);

            // Merge All the files and upload to s3
            if (pdfMerge.s3Filenames.size() > 0) {
                pdfMerge.mergePDFAndUploadToS3();
                // Delete all the files downloaded on the local machine
                logger.info("Deleting the downloaded files ");
                S3Utility.deleteObjectsInPath(localOutputPath);
                S3Utility.deleteObjectsInPath(localInputPath);
            }
            logger.info("Process completed for claim number : " + config.getClaimNumber());
            long duration = (System.nanoTime() - startTime);
            logger.info("duration in milli sec: " + duration / 1000000);
            successStatus = true;
        } catch (OutOfMemoryError outOfMemoryError) {
            logger.error("OUT OF MEMORY ERROR FOR: " + config.getClaimNumber(), outOfMemoryError,
                    outOfMemoryError.getStackTrace());
            successStatus = false;
        } catch (IOException ioException) {
            logger.error("Exception Occured: ", ioException, ioException.getStackTrace());
            successStatus = false;
        } catch (CsvValidationException csvValidationException) {
            logger.error("Exception Occured: ", csvValidationException, csvValidationException.getStackTrace());
            successStatus = false;
        } catch (Exception exception) {
            logger.error("Exception Occured: ", exception, exception.getStackTrace());
            successStatus = false;
        } finally {
            // Push notification to an AWS SNS topic along with the receipt handler of current item and claim number
            // processed for deletion from queue

            AmazonSNS snsClient = AmazonSNSClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
            final SNSMessageAttributes message = new SNSMessageAttributes("message");
            message.addAttribute("receiptHandle", config.getReceiptHandle());
            message.addAttribute("claimNumber", config.getClaimNumber());
            if (successStatus) {
                message.addAttribute("updatedFiles", pdfMerge.s3Filenames.keySet().toString());
                message.addAttribute("executionStatus", "SUCCESS");
            } else {
                message.addAttribute("updatedFiles", "[]");
                message.addAttribute("executionStatus", "FAILED");
            }
            message.addAttribute("tenant", config.getTenant());
            message.addAttribute("requestId", config.getRequestId());
            message.addAttribute("s3OutputPath", config.getOutputS3Path());
            message.publish(snsClient, config.getTopicArn());
            logger.info("-------------------------------------------------------------------------");
        }
    }
}
