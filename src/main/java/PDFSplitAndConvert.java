import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import static constants.Constants.PDF_EXT;
import static constants.Constants.SLASH;
import static constants.Constants.TEN;
import static constants.Constants.TIF;
import static constants.Constants.TIFF;

public class PDFSplitAndConvert {

    private static AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
    private static Logger logger = LogManager.getLogger(PDFSplitAndConvert.class);
    private String localInputPath;
    private String inputBucket;
    private String localOutputPath;
    private String outputBucket;
    private String s3OutputPath;
    private String medicalImageBkt;
    //key = year and list= list of files
    Map<String, List<String>> s3Filenames = new HashMap<>();
    private boolean isSorted = false;

    public PDFSplitAndConvert(String localInputPath, String inputBucket, String localOutputPath, String outputBucket,
            String s3OutputPath, String medicalImageBkt) {
        this.localInputPath = localInputPath;
        this.inputBucket = inputBucket;
        this.localOutputPath = localOutputPath;
        this.outputBucket = outputBucket;
        this.s3OutputPath = s3OutputPath;
        this.medicalImageBkt = medicalImageBkt;
    }

    /*
   This method takes the list of s3 file names and downloads then to the local directory
   use when file name in  <claim-number>-timestamp-uuid format or need to extract the dated order via pattern matching
   * */
    public void downloadFiles(List<S3ObjectSummary> summaries) {
        ExecutorService downloadExecutor = getExecutors(TEN);
        for (S3ObjectSummary summary : summaries) {
            String s3FileName = summary.getKey();
            String[] filepaths = s3FileName.split(SLASH);
            String filename = filepaths[filepaths.length - 1];
            if (filename.endsWith(PDF_EXT) || filename.toUpperCase().endsWith(TIF) || filename.toUpperCase()
                    .endsWith(TIFF)) {
                DownloadManager downloadManager =
                        new DownloadManager(s3FileName, localInputPath + filename, inputBucket);
                downloadExecutor.execute(downloadManager);
            }
        }
        waitForExecutorCompletion(downloadExecutor);
    }

    /* Initialize an executor service
     * */
    public ExecutorService getExecutors(int noOfThreads) {
        return Executors.newFixedThreadPool(noOfThreads);
    }

    /* Shutdown and wait for the completion of the ExecutorService
     * */
    public void waitForExecutorCompletion(ExecutorService executor) {
        executor.shutdown();
        while (!executor.isTerminated()) {

        }
    }
}
