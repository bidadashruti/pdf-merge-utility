/*
 *  @author: shruti.bidada
 * */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.multipdf.PDFMergerUtility;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import util.S3Utility;

/*
 * This <code>PDFMergeManager</code> is a runnable class to do the following operations:
 * 1. Merge All the documents passed in the filenames parameter
 * 2. Check for the presence of the output file on s3 and merge that to the final output
 * 3. Upload the file to s3OutputBucket/s3OutputPath
 * */
public class PDFSplitAndConvertManager implements Runnable {

    private static Logger logger = LogManager.getLogger(PDFSplitAndConvertManager.class);
    private static final AmazonS3 S3_CLIENT = AmazonS3ClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
    private String localInputPath;
    private String localOutputPath;
    private String s3OutputBucket;
    private String s3OutputPath;
    private String pdfFileName;
    private List<String> filenames;

    public PDFSplitAndConvertManager(String localInputPath, String localOutputPath, String outputBucket, String s3OutputPath,
            String pdfFileName, List<String> s3Filenames) {
        this.localInputPath = localInputPath;
        this.localOutputPath = localOutputPath;
        this.s3OutputBucket = outputBucket;
        this.s3OutputPath = s3OutputPath;
        this.pdfFileName = pdfFileName;
        this.filenames = s3Filenames;
    }

    @Override
    public void run() {
        PDFMergerUtility mergeUtil = new PDFMergerUtility();
        // Adding all the files to the merge util
        for (String fileName : filenames) {
            try {
                mergeUtil.addSource(localInputPath + fileName);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        // Checking for the existing file for the relevant year and downloading and adding to mergeutil if present
        if (S3_CLIENT.doesObjectExist(s3OutputBucket, s3OutputPath)) {
            logger.info("Merged PDF exists");
            File localFile = new File(localInputPath + pdfFileName + ".pdf");
            S3_CLIENT.getObject(new GetObjectRequest(s3OutputBucket, s3OutputPath), localFile);
            try {
                mergeUtil.addSource(localInputPath + pdfFileName + ".pdf");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        // Calling merge action
        logger.info("Merging and storing the files at " + localOutputPath);
        S3Utility.createNewDirectory(localOutputPath);
        mergeUtil.setDestinationFileName(localOutputPath + pdfFileName + ".pdf");
        mergeUtil.setDestinationStream(null);
        try {
            mergeUtil.mergeDocuments();
        } catch (IOException e) {
            logger.info("Issue while processing ::" + localOutputPath + pdfFileName + ".pdf");
            e.printStackTrace();
        }

        S3_CLIENT.putObject(s3OutputBucket, s3OutputPath, new File(localOutputPath + pdfFileName + ".pdf"));
        logger.info("Merging complete");
        logger.info("Uploaded file to " + s3OutputPath);
    }
}
