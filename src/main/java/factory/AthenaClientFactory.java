package factory;/*
 * Copyright 2017- 2020 CLARA analytics, Inc., 451 El Camino Real, Suite 201, Santa Clara CA USA 95050.
 * All Rights Reserved. All information contained herein is, and remains
 * the property of CLARA analytics, Inc. The intellectual and technical concepts contained herein are proprietary to
 * CLARA analytics, Inc. and may be covered by U.S. Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from CLARA analytics, Inc.
 */
/*

 * @author <a href="abhijith.n@claraanalytics.com">Abhijith Nagaraja</a>

 * @version $$Revision: 1.0 $$, $$Date: 4/7/20.

 */

import com.amazonaws.regions.Regions;
import com.amazonaws.services.athena.AmazonAthena;
import com.amazonaws.services.athena.AmazonAthenaClientBuilder;

public final class AthenaClientFactory {

    /**
     * AmazonAthenaClientBuilder to build Athena with the following properties: - Set the region of the client - Use the
     * instance profile from the EC2 instance as the credentials provider - Configure the client to increase the
     * execution timeout.
     */
    private static AmazonAthena client;

    private AthenaClientFactory(Regions region) {
        client = createClient(region);
    }

    private AmazonAthena createClient(Regions region) {
        return AmazonAthenaClientBuilder.standard().withRegion(region).build();
    }

    public static AmazonAthena getClient(Regions region) {
        if (client == null) {
            new AthenaClientFactory(region);
        }

        return client;
    }
}