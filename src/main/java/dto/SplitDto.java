package dto;

public class SplitDto {
 String pdfPageFile;
 String jpegPageFile;

    public SplitDto(String pdfPageFile, String jpegPageFile) {
        this.pdfPageFile = pdfPageFile;
        this.jpegPageFile = jpegPageFile;
    }

    public String getPdfPageFile() {
        return pdfPageFile;
    }

    public void setPdfPageFile(String pdfPageFile) {
        this.pdfPageFile = pdfPageFile;
    }

    public String getJpegPageFile() {
        return jpegPageFile;
    }

    public void setJpegPageFile(String jpegPageFile) {
        this.jpegPageFile = jpegPageFile;
    }
}
