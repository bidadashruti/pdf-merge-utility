/*
 * Copyright 2017- 2020 CLARA analytics, Inc., 451 El Camino Real, Suite 201, Santa Clara CA USA 95050.
 * All Rights Reserved. All information contained herein is, and remains
 * the property of CLARA analytics, Inc. The intellectual and technical concepts contained herein are proprietary to
 * CLARA analytics, Inc. and may be covered by U.S. Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from CLARA analytics, Inc.
 */
package util;
/*

 * @author <a href="abhijith.n@claraanalytics.com">Abhijith Nagaraja</a>

 * @version $$Revision: 1.0 $$, $$Date: 4/10/20.

 */

import java.io.IOException;

import org.springframework.core.io.ClassPathResource;

import com.amazonaws.util.IOUtils;

public class ClassPathUtil {

    public static String getTextfileContent(final String filename) throws IOException {
        return IOUtils.toString(new ClassPathResource(filename).getInputStream());
    }
}
