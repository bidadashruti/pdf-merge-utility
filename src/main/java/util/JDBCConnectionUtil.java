package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JDBCConnectionUtil {

    private Connection connection;
    private final Logger logger;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public JDBCConnectionUtil(String url, String username, String password, String dbname) {

        this.logger = LogManager.getLogger(JDBCConnectionUtil.class.getName());

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url + dbname, username, password);
            logger.info("Connection successful");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ResultSet executeQuery(String query) {
        ResultSet rs = null;
        try {
            Statement statement = this.connection.createStatement();
            rs = statement.executeQuery(query);
            logger.info("executeElasticQuery::Completed Executing MySQL Query");
        } catch (SQLException e) {
            logger.error("ERROR in executing query " + e.getMessage());
        }
        return rs;
    }

    public int executeUpdate(String updateQuery) {
        int rs = 0;
        try {
            Statement statement = this.connection.createStatement();
            rs = statement.executeUpdate(updateQuery);
            logger.info("executeElasticQuery::Completed Executing MySQL Query");
        } catch (SQLException e) {
            logger.error("ERROR in executing query " + e.getMessage());
        }
        return rs;
    }
}
