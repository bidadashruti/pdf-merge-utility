package util;/*
 * Copyright 2017- 2020 CLARA analytics, Inc., 451 El Camino Real, Suite 201, Santa Clara CA USA 95050.
 * All Rights Reserved. All information contained herein is, and remains
 * the property of CLARA analytics, Inc. The intellectual and technical concepts contained herein are proprietary to
 * CLARA analytics, Inc. and may be covered by U.S. Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from CLARA analytics, Inc.
 */
/*

 * @author <a href="abhijith.n@claraanalytics.com">Abhijith Nagaraja</a>

 * @version $$Revision: 1.0 $$, $$Date: 6/9/20.

 */

import java.util.HashMap;
import java.util.Map;

public final class MapUtil {

    /**
     * This method creates a map for give keys and string
     *
     * @param keys
     * @param values
     * @return
     */
    public static Map<String, String> buildKeyValueMap(String[] keys, String[] values) {
        int length = keys.length;

        Map<String, String> keyValueMap = new HashMap<>();

        for (int i = 0; i < length; i++) {
            if (values[i] != null && values[i].equalsIgnoreCase("NULL")) {
                values[i] = null;
            }
            keyValueMap.put(keys[i], values[i]);
        }

        return keyValueMap;
    }
}
