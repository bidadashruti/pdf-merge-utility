/*
 * Copyright 2017- 2020 CLARA analytics, Inc., 451 El Camino Real, Suite 201, Santa Clara CA USA 95050.
 * All Rights Reserved. All information contained herein is, and remains
 * the property of CLARA analytics, Inc. The intellectual and technical concepts contained herein are proprietary to
 * CLARA analytics, Inc. and may be covered by U.S. Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from CLARA analytics, Inc.
 */
package util;
/*

 * @author <a href="abhijith.n@claraanalytics.com">Abhijith Nagaraja</a>

 * @version $$Revision: 1.0 $$, $$Date: 4/10/20.

 */

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import config.Config;

public class ElasticServiceUtil {

    private static Logger LOGGER = LogManager.getLogger();
    private final String[] HOSTS = new String[]{"elasticmaster01", "elasticmaster02", "elasticmaster03"};
    private final String CLUSTER_NAME = "clara-cluster";
    private final int TRANSPORT_PORT = 9300;
    private final int MAX_RETIRES = 3;

    public TransportClient getClient() {
        return client;
    }

    public void setClient(TransportClient client) {
        this.client = client;
    }

    private TransportClient client = null;
    private Config config;

    public void startConnection() {
        String[] hosts;
        String clusterName;

        hosts = HOSTS;
        clusterName = CLUSTER_NAME;
        try {
            this.client = new PreBuiltTransportClient(
                    Settings.builder().put("client.transport.sniff", hosts.length > 1).put("cluster.name", clusterName)
                            .build());

            for (String host : hosts) {
                client.addTransportAddress(new TransportAddress(InetAddress.getByName(host), TRANSPORT_PORT));
            }
        } catch (UnknownHostException e) {
            ExceptionUtil.logException(e, "Elastic host not found", LOGGER);
        } catch (Exception e) {
            ExceptionUtil.logException(e, "Unable to talk to ElasticSearch host", LOGGER);
        }
    }

    public void closeConnection() {
        client.close();
    }

    public boolean indexExists(String indexName) {
        return client.admin().indices().prepareExists(indexName).execute().actionGet().isExists();
    }

    public void createElasticIndex(String indexName, String docType, String mappingFilename, String settingsFilename) {
        createElasticIndex(indexName, docType, mappingFilename, settingsFilename, 0);
    }

    private void createElasticIndex(String indexName, String docType, String mappingFilename, String settingsFilename,
            int retryCount) {
        if (indexExists(indexName)) {
            ExceptionUtil.logException(indexName + "already exists", LOGGER);
        } else {
            CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate(indexName);
            if (StringUtils.isNotEmpty(settingsFilename)) {
                createIndexRequestBuilder.setSettings(Settings.builder());
            }
            createIndexRequestBuilder.execute().actionGet();
            LOGGER.info("Elastic Index " + indexName + " created");
            LOGGER.info("Mapping applied");
            if (StringUtils.isNotEmpty(settingsFilename)) {
                LOGGER.info("Settings applied");
            }
            verifyAndRetryIndexCreation(indexName, docType, mappingFilename, settingsFilename, retryCount);
        }
    }

    private void verifyAndRetryIndexCreation(String indexName, String docType, String mappingFilename,
            String settingsFilename, int retryCount) {
        if (indexExists(indexName)) {
            LOGGER.info("Index creation verified");
        } else {
            if (retryCount < MAX_RETIRES) {
                createElasticIndex(indexName, docType, mappingFilename, settingsFilename, ++retryCount);
            } else {
                ExceptionUtil.logException("Index cannot be created. Something went wrong");
            }
        }
    }

    public void deleteIndex(String indexName) {
        client.admin().indices().prepareDelete(indexName).execute().actionGet();
        LOGGER.info("Index Deleted: " + indexName);
    }

    public boolean aliasExists(String aliasName) {
        return client.admin().indices().prepareAliasesExist(aliasName).execute().actionGet().isExists();
    }

}
