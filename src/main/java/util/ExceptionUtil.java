package util;
/*
 * Copyright 2017- 2020 CLARA analytics, Inc., 451 El Camino Real, Suite 201, Santa Clara CA USA 95050.
 * All Rights Reserved. All information contained herein is, and remains
 * the property of CLARA analytics, Inc. The intellectual and technical concepts contained herein are proprietary to
 * CLARA analytics, Inc. and may be covered by U.S. Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from CLARA analytics, Inc.
 */
/*

 * @author <a href="abhijith.n@claraanalytics.com">Abhijith Nagaraja</a>

 * @version $$Revision: 1.0 $$, $$Date: 4/11/20.

 */

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ExceptionUtil {

    private static Logger LOGGER = LogManager.getLogger();

    public static void logException(String message) {
        logException(null, message);
    }

    public static void logException(String message, Logger logger) {
        logException(null, message, logger);
    }

    public static void logException(Exception e) {
        logException(e, null, LOGGER);
    }

    public static void logException(Exception e, Logger logger) {
        logException(e, null, logger);
    }

    public static void logException(Exception e, String message) {
        logException(e, message, LOGGER);
    }

    public static void logException(Exception e, String message, Logger logger) {
        String msg;
        if (StringUtils.isNotEmpty(message)) {
            msg = message;
        } else {
            msg = e.getMessage();
        }

        if (logger == null) {
            logger = LOGGER;
        }
        logger.error(msg, e);
        System.exit(1);
    }
}
