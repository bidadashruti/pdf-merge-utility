package util;
/*
 *  @author: shruti.bidada
 * */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;

/*
 * This <code>util.S3Utility</code> Class is to add the reusable s3 utility methods
 * */
public class S3Utility {

    private static Logger LOGGER = LogManager.getLogger();

    /*
     * Method to paginate and list all the s3 objects for a prefix path
     *
     * @return A list of the object summaries describing the objects stored in
     *         the S3 bucket.
     * */
    public static List<S3ObjectSummary> getS3ObjectList(AmazonS3 s3client, String bucket, String s3Prefix) {
        List<S3ObjectSummary> keyList = new ArrayList<S3ObjectSummary>();
        ObjectListing objectList =
                s3client.listObjects(new ListObjectsRequest().withBucketName(bucket).withPrefix(s3Prefix));

        keyList = objectList.getObjectSummaries();
        objectList = s3client.listNextBatchOfObjects(objectList);

        while (objectList.isTruncated()) {
            keyList.addAll(objectList.getObjectSummaries());
            objectList = s3client.listNextBatchOfObjects(objectList);
        }
        keyList.addAll(objectList.getObjectSummaries());
        return keyList;
    }

    /* Create a local directory if not exists
     * */
    public static void createNewDirectory(String directoryPath) {
        File directory = new File(directoryPath);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    public static void deleteObjectsInPath(String path) throws IOException {
        Files.walk(Paths.get(path)).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
    }

    private static AmazonS3 getS3Client(Regions region) {
        return AmazonS3ClientBuilder.standard().withRegion(region).build();
    }

    /**
     * Method to read the file from s3 and write to the execution machine
     *
     * @param filename   - input filename
     * @param region     - AWS region
     * @param bucket     - s3 bucket to read the file from
     * @param inputPath  - s3 path to file excluding the name
     * @param outputPath - output path without the filename
     */
    public static void read(String filename, String region, String bucket, String inputPath, String outputPath) {

        AmazonS3 s3client = getS3Client(AwsUtil.getRegion(region));
        LOGGER.info("Reading from S3:: " + inputPath);

        S3Object fullObject = s3client.getObject(new GetObjectRequest(bucket, inputPath));
        InputStream objectData = fullObject.getObjectContent();
        BufferedReader br = new BufferedReader(new InputStreamReader(objectData));
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            File newDirectory = new File(outputPath);
            if (!newDirectory.exists()) {
                newDirectory.mkdirs();
            }

            File fileTobeSaved = new File(outputPath, filename);

            fw = new FileWriter(fileTobeSaved);
            bw = new BufferedWriter(fw);

            String strCurrentLine;
            while ((strCurrentLine = br.readLine()) != null) {
                bw.write(strCurrentLine);
                bw.write("\n");
            }

            LOGGER.info("File is available at " + outputPath + filename);
        } catch (IOException e) {
            ExceptionUtil.logException(e, "Error while reading file from S3", LOGGER);
        } finally {
            try {
                fullObject.close();
                objectData.close();
                br.close();
                if (bw != null) {
                    bw.close();
                }
                if (fw != null) {
                    fw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
