package util;
/*

 * @author <a href="abhijith.n@claraanalytics.com">Abhijith Nagaraja</a>

 * @version $$Revision: 1.0 $$, $$Date: 6/8/20.

 */

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.regions.Regions;

public final class AwsUtil {

    public static Regions getRegion(String region) {
        Regions reg;
        if (StringUtils.isEmpty(region)) {
            reg = Regions.US_WEST_2;
        } else {
            reg = Regions.fromName(region);
        }
        return reg;
    }

    public static String getRegionString(String region) {
        Regions reg;
        if (StringUtils.isEmpty(region)) {
            reg = Regions.US_WEST_2;
        } else {
            reg = Regions.fromName(region);
        }
        return reg.getName();
    }
}
