package util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.services.athena.model.GetQueryExecutionRequest;
import com.amazonaws.services.athena.model.GetQueryExecutionResult;
import com.amazonaws.services.athena.model.QueryExecutionContext;
import com.amazonaws.services.athena.model.QueryExecutionState;
import com.amazonaws.services.athena.model.ResultConfiguration;
import com.amazonaws.services.athena.model.StartQueryExecutionRequest;
import com.amazonaws.services.athena.model.StartQueryExecutionResult;
import factory.AthenaClientFactory;

public class AthenaUtil {

    public static final String DOWNLOAD_PATH = "/tmp/";
    public static final int DEFAULT_RETRY_WAIT_TIME_IN_MS = 1000;
    public static final String QUERY_OUTPUT_PATH = "/processed/athena-query-output/";
    private static Logger LOGGER = LogManager.getLogger();

    public static String submitAndDownloadQueryResults(String region, String bucket, String tenant, String product,
            String database, String query) {
        String id = AthenaUtil.submitAthenaQuery(region, bucket, tenant, product, database, query);
        LOGGER.info("Triggered query in Athena. Waiting for query processing to complete");
        try {
            AthenaUtil.waitForQueryToComplete(id, region);
        } catch (InterruptedException e) {
            ExceptionUtil.logException(e, "Error while waiting for query to complete", LOGGER);
        }
        LOGGER.info("Query Completed. Downloading to results");
        String filename = id + ".csv";
        S3Utility.read(filename, region, bucket, tenant + QUERY_OUTPUT_PATH + product + "/", DOWNLOAD_PATH);
        return DOWNLOAD_PATH + filename;
    }

    public static String submitAthenaQuery(String region, String bucket, String tenant, String product, String database,
            String query) {
        String S3_QUERY_OUTPUT_LOCATION = "s3://" + bucket + "/" + tenant + QUERY_OUTPUT_PATH + product + "/";

        QueryExecutionContext queryExecutionContext = new QueryExecutionContext().withDatabase(database);

        // The result configuration specifies where the results of the query should go in S3 and encryption options
        ResultConfiguration resultConfiguration =
                new ResultConfiguration().withOutputLocation(S3_QUERY_OUTPUT_LOCATION);

        // Create the StartQueryExecutionRequest to send to Athena which will start the query.
        StartQueryExecutionRequest startQueryExecutionRequest =
                new StartQueryExecutionRequest().withQueryString(query).withQueryExecutionContext(queryExecutionContext)
                        .withResultConfiguration(resultConfiguration);

        StartQueryExecutionResult startQueryExecutionResult = AthenaClientFactory.getClient(AwsUtil.getRegion(region))
                .startQueryExecution(startQueryExecutionRequest);
        return startQueryExecutionResult.getQueryExecutionId();
    }

    /**
     * Wait for an Athena query to complete, fail or to be cancelled. This is done by polling Athena over an interval of
     * time. If a query fails or is cancelled, then it will throw an exception.
     */
    public static void waitForQueryToComplete(String queryExecutionId, String region) throws InterruptedException {
        GetQueryExecutionRequest getQueryExecutionRequest =
                new GetQueryExecutionRequest().withQueryExecutionId(queryExecutionId);

        GetQueryExecutionResult getQueryExecutionResult;
        boolean isQueryStillRunning = true;
        boolean isQueryQueued = true;
        while (isQueryStillRunning) {
            getQueryExecutionResult = AthenaClientFactory.getClient(AwsUtil.getRegion(region))
                    .getQueryExecution(getQueryExecutionRequest);
            String queryState = getQueryExecutionResult.getQueryExecution().getStatus().getState();
            if (isQueryQueued) {
                if (queryState.equals(QueryExecutionState.QUEUED.toString())) {
                    // Sleep an amount of time before retrying again.
                    Thread.sleep(DEFAULT_RETRY_WAIT_TIME_IN_MS);
                } else {
                    isQueryQueued = false;
                }
            } else if (queryState.equals(QueryExecutionState.SUCCEEDED.toString())) {
                isQueryStillRunning = false;
            } else if (queryState.equals(QueryExecutionState.CANCELLED.toString())) {
                throw new RuntimeException("Query was cancelled.");
            } else if (queryState.equals(QueryExecutionState.FAILED.toString())) {
                throw new RuntimeException(
                        "Query Failed to run with Error Message: " + getQueryExecutionResult.getQueryExecution()
                                .getStatus().getStateChangeReason());
            } else {
                // Sleep an amount of time before retrying again.
                Thread.sleep(DEFAULT_RETRY_WAIT_TIME_IN_MS);
            }
        }
    }
}
