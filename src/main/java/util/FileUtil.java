package util;/*
 * Copyright 2017- 2020 CLARA analytics, Inc., 451 El Camino Real, Suite 201, Santa Clara CA USA 95050.
 * All Rights Reserved. All information contained herein is, and remains
 * the property of CLARA analytics, Inc. The intellectual and technical concepts contained herein are proprietary to
 * CLARA analytics, Inc. and may be covered by U.S. Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from CLARA analytics, Inc.
 */
/*

 * @author <a href="shruti.b@claraanalytics.com">Shruti Bidada</a>

 * @version $$Revision: 1.0 $$, $$Date: 6/8/20.

 */

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.RandomAccessFileOrArray;
import com.itextpdf.text.pdf.codec.TiffImage;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.RFC4180Parser;
import com.opencsv.RFC4180ParserBuilder;
import dto.SplitDto;

import static constants.Constants.FIFTY;
import static constants.Constants.JPEG_EXT;
import static constants.Constants.PDF_EXT;

public final class FileUtil {

    private static Logger LOGGER = LogManager.getLogger();

    /**
     * Delete the file present in the local machine
     *
     * @param filename
     */
    public static void deleteDownloadedFile(String filename) {
        File file = new File(filename);
        if (file.delete()) {
            LOGGER.info(filename + " deleted");
        } else {
            LOGGER.error("Error in deleting the file " + filename);
        }
        LOGGER.info("Downloaded file" + filename + " Deleted");
    }

    //Athena encodes csv in RFC4180 standard and hence normal parsers cannot be used as some of cases fail
    public static CSVReader getCSVReader(String filename) {
        RFC4180Parser rfc4180Parser = new RFC4180ParserBuilder().build();
        try {
            return new CSVReaderBuilder(new FileReader(filename)).withCSVParser(rfc4180Parser).build();
        } catch (FileNotFoundException e) {
            LOGGER.error("FileNotFoundException");
            ExceptionUtil.logException(e, "Error while reading the file", LOGGER);
        } catch (Exception exp) {
            LOGGER.error("Exception");
        }
        return null;
    }

    /**
     * Method to convert tiff files to PDF using ITEXT.
     * - Read the file
     * - Check the no of pages
     * - Convert and append page to the resulting pdf
     * NOTE: adding a 50px vertical and horizontal padding to prevent page cropping
     *
     * @param filename       - local input file path with name
     * @param outputFilePath - local output file path with name
     */
    public static void convertTiffToPDF(String filename, String outputFilePath) {
        Document tiffToPDF = null;
        try {
            LOGGER.info("convertTiffToPDF :: Starting to convert file :: " + filename);

            //Read the Tiff File
            RandomAccessFileOrArray myTiffFile = new RandomAccessFileOrArray(filename);
            //Find number of images in Tiff file
            int numberOfPages = TiffImage.getNumberOfPages(myTiffFile);

            tiffToPDF = new Document();
            PdfWriter.getInstance(tiffToPDF, new FileOutputStream(outputFilePath));
            tiffToPDF.open();

            //Run a for loop to extract images from Tiff file
            //into a Image object and add to PDF recursively
            for (int page = 1; page <= numberOfPages; page++) {
                Image tempImage = TiffImage.getTiffImage(myTiffFile, page);
                //Appending 50px to avoid trimmed pages
                Rectangle pageSize = new Rectangle(tempImage.getWidth() + FIFTY, tempImage.getHeight() + FIFTY);
                tiffToPDF.setPageSize(pageSize);
                tiffToPDF.newPage();
                tiffToPDF.add(tempImage);
            }
            tiffToPDF.close();
            LOGGER.info("convertTiffToPDF :: File conversion completed :: " + outputFilePath);
        } catch (Exception ex) {
            LOGGER.error("convertTiffToPDF :: Hit Exception while converting " + filename + "::", ex.toString());
            ex.printStackTrace();
        } finally {
            if (tiffToPDF != null) {
                tiffToPDF.close();
            }
        }
    }

    /**
     * Method to spilt the pdf
     *
     * @param source:     local source path
     * @param destination : local destination path
     * @param extension   : extension ex: .pdf
     */
    public static List<SplitDto> splitPdf(String source, String destination, String extension)
            throws IOException {
        List<SplitDto> result = new ArrayList<>();
        File file = new File(source);
        PDDocument doc = null;
        doc = PDDocument.load(file);

        //Instantiating Splitter class
        Splitter splitter = new Splitter();

        //splitting the pages of a PDF document
        List<PDDocument> Pages = null;
        Pages = splitter.split(doc);

        //Creating an iterator
        Iterator<PDDocument> iterator = Pages.listIterator();

        //Saving each page as an individual document
        int i = 1;

        destination = destination.replace(PDF_EXT, "") + "_page";
        while (iterator.hasNext()) {
            PDDocument pd = iterator.next();
            String pdfPageName = destination + i + extension;
            pd.save(pdfPageName);
            pd.close();

            //TODO: Adjust the output paths
            String jpegPageName = destination + i + JPEG_EXT;
            PDFtoJPEG(pdfPageName, jpegPageName);
            LOGGER.info("Adding "+ pdfPageName +" and "+ jpegPageName);
            result.add(new SplitDto(pdfPageName,jpegPageName));
            i++;
        }
        LOGGER.info("PDF Split Completed for :: " + source);
        return result;
    }

    /**
     * Method to convert pdf to jpeg
     *
     * @param inputPath:  local input path
     * @param outputPath: local output path
     * @throws Exception
     */
    public static void PDFtoJPEG(String inputPath, String outputPath) throws IOException {
        PDDocument pd = PDDocument.load(new File(inputPath));
        PDFRenderer pr = new PDFRenderer(pd);
        BufferedImage bi = pr.renderImageWithDPI(0, 300);
        ImageIO.write(bi, "JPEG", new File(outputPath));
        pd.close();
    }
}
