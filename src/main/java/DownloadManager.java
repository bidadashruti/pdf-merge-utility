/*
 *  @author: shruti.bidada
 * */

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import util.FileUtil;

import static constants.Constants.*;

/*
 * This <code>DownloadManager</code> is a runnable class to download the files from s3
 * */
public class DownloadManager implements Runnable {

    private static Logger logger = LogManager.getLogger(PDFMergeManager.class);
    String s3Path;
    String localFile;
    String bucket;
    String localSplitOutputPath;
    String outputBucket;
    private static AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_WEST_2).build();

    protected DownloadManager(String s3Path, String localFile, String bucket) {
        this.s3Path = s3Path;
        this.localFile = localFile;
        this.bucket = bucket;
    }

    @Override
    public void run() {
        if (s3client.doesObjectExist(this.bucket, this.s3Path)) {
            File localFile = new File(this.localFile);
            s3client.getObject(new GetObjectRequest(this.bucket, this.s3Path), localFile);

            //if file is tiff, then convert to pdf
            if (this.localFile.matches(".*" + TIFF_FILE_EXT_REGEX)) {
                FileUtil.convertTiffToPDF(this.localFile, this.localFile.replaceFirst(TIFF_FILE_EXT_REGEX, PDF_EXT));
                FileUtil.deleteDownloadedFile(this.localFile);
            }
        } else {
            logger.warn("S3:Object does not exists:" + this.bucket + SLASH + this.s3Path);
        }
    }
}