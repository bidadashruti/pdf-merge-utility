package config;

public class Config {

    private String claimNumber;
    private String requestId;
    private String msaId;
    private String tenant;
    private String inventoryBucket;
    private String inventoryS3Path;
    private String dataBucketName;
    private String outputBucket;
    private String outputS3Path;
    private String receiptHandle;
    private String localInputPath;
    private String localOutputPath;
    private String topicArn;
    private String action;
    private String secretName;
    private String indexName;
    private String s3Key;

    public String getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(String claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMsaId() {
        return msaId;
    }

    public void setMsaId(String msaId) {
        this.msaId = msaId;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getInventoryBucket() {
        return inventoryBucket;
    }

    public void setInventoryBucket(String inventoryBucket) {
        this.inventoryBucket = inventoryBucket;
    }

    public String getInventoryS3Path() {
        return inventoryS3Path;
    }

    public void setInventoryS3Path(String inventoryS3Path) {
        this.inventoryS3Path = inventoryS3Path;
    }

    public String getDataBucketName() {
        return dataBucketName;
    }

    public void setDataBucketName(String dataBucketName) {
        this.dataBucketName = dataBucketName;
    }

    public String getOutputBucket() {
        return outputBucket;
    }

    public void setOutputBucket(String outputBucket) {
        this.outputBucket = outputBucket;
    }

    public String getOutputS3Path() {
        return outputS3Path;
    }

    public void setOutputS3Path(String outputS3Path) {
        this.outputS3Path = outputS3Path;
    }

    public String getReceiptHandle() {
        return receiptHandle;
    }

    public void setReceiptHandle(String receiptHandle) {
        this.receiptHandle = receiptHandle;
    }

    public String getLocalInputPath() {
        return localInputPath;
    }

    public void setLocalInputPath(String localInputPath) {
        this.localInputPath = localInputPath;
    }

    public String getLocalOutputPath() {
        return localOutputPath;
    }

    public void setLocalOutputPath(String localOutputPath) {
        this.localOutputPath = localOutputPath;
    }

    public String getTopicArn() {
        return topicArn;
    }

    public void setTopicArn(String topicArn) {
        this.topicArn = topicArn;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getSecretName() {
        return secretName;
    }

    public void setSecretName(String secretName) {
        this.secretName = secretName;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getS3Key() {
        return s3Key;
    }

    public void setS3Key(String s3Key) {
        this.s3Key = s3Key;
    }
}
