import java.io.FileNotFoundException;
import java.time.Year;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import config.Config;
import util.JDBCConnectionUtil;

import static constants.Constants.FOUR;
import static constants.Constants.PDF_EXT;
import static constants.Constants.SLASH;
import static constants.Constants.TEN;
import static constants.Constants.TIF;
import static constants.Constants.TIFF;
import static constants.Constants.TIFF_FILE_EXT_REGEX;
import static constants.Constants.UNKNOWN;

public class PDFMerge {

    private static AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.US_WEST_2).build();
    private static Logger logger = LogManager.getLogger(PDFMerge.class);
    //The year on file should be between 1940 and current year
    private static final int LOWER_YEAR_THRESHOLD = 1940;
    private static final int UPPER_YEAR_THRESHOLD = Year.now().getValue();
    private String localInputPath;
    private String inputBucket;
    private String localOutputPath;
    private String outputBucket;
    private String s3OutputPath;
    private String medicalImageBkt;
    private JDBCConnectionUtil jdbcConnection;
    //key = year and list= list of files
    Map<String, List<String>> s3Filenames = new HashMap<>();
    List<String> updatedFileNameList = new ArrayList<>();
    private boolean isSorted = false;

    public PDFMerge(String localInputPath, String inputBucket, String localOutputPath, String outputBucket,
            String s3OutputPath, String medicalImageBkt) {
        this.localInputPath = localInputPath;
        this.inputBucket = inputBucket;
        this.localOutputPath = localOutputPath;
        this.outputBucket = outputBucket;
        this.s3OutputPath = s3OutputPath;
        this.medicalImageBkt = medicalImageBkt;
    }

    public boolean isSorted() {
        return isSorted;
    }

    public void setSorted(boolean sorted) {
        isSorted = sorted;
    }

    public JDBCConnectionUtil getJdbcConnection() {
        return jdbcConnection;
    }

    public void setJdbcConnection(JDBCConnectionUtil jdbcConnection) {
        this.jdbcConnection = jdbcConnection;
    }

    /*
    This method takes the list of s3 file names and downloads then to the local directory
    use when file name in  <claim-number>-timestamp-uuid format or need to extract the dated order via pattern matching
    * */
    public void downloadFilesUsingPattern(List<S3ObjectSummary> summaries, Pattern pattern) {
        ExecutorService downloadExecutor = getExecutors(TEN);
        for (S3ObjectSummary summary : summaries) {
            String s3FileName = summary.getKey();
            String[] filepaths = s3FileName.split(SLASH);
            String filename = filepaths[filepaths.length - 1];
            if (filename.endsWith(PDF_EXT)) {
                Matcher matcher = pattern.matcher(filename);
                List files = null;
                String fileKey = matcher.find() ? matcher.group() : null;
                if (fileKey != null) {
                    files = s3Filenames.getOrDefault(fileKey, new ArrayList<>());
                    files.add(filename);
                    s3Filenames.put(fileKey, files);
                }
                DownloadManager downloadManager =
                        new DownloadManager(s3FileName, localInputPath + filename, inputBucket);
                downloadExecutor.execute(downloadManager);
            }
        }
        waitForExecutorCompletion(downloadExecutor);
    }

    /*
    This method takes the list of s3 file names and downloads them to the local directory
    Use this when date is not in the filename and is being added as the value in map
    * */
    public void downloadFiles(Map<String, String> summaries, String claimNumber) throws FileNotFoundException {
        ExecutorService downloadExecutor = getExecutors(TEN);
        List files = null;

        for (String s3FileName : summaries.keySet()) {
            logger.info("File name ::" + s3FileName);
            String[] filePaths = s3FileName.split("/");
            String filename = filePaths[filePaths.length - 1];

            //Date in yyyy-mm-dd format, hence extracting the year and checking for year to be valid
            String fileYear = LOWER_YEAR_THRESHOLD <= Integer.parseInt(summaries.get(s3FileName).substring(0, 4))
                    && Integer.parseInt(summaries.get(s3FileName).substring(0, 4)) <= UPPER_YEAR_THRESHOLD ? summaries
                    .get(s3FileName).substring(0, 4) : UNKNOWN;
            String fileKey = claimNumber + "-" + fileYear;
            if (s3client.doesObjectExist(medicalImageBkt, s3FileName)) {
                if (filename.endsWith(PDF_EXT) || filename.toUpperCase().endsWith(TIF) || filename.toUpperCase()
                        .endsWith(TIFF)) {
                    files = s3Filenames.getOrDefault(fileKey, new ArrayList<>());
                    files.add(filename.replaceFirst(TIFF_FILE_EXT_REGEX, PDF_EXT));
                    s3Filenames.put(fileKey, files);

                    DownloadManager downloadManager =
                            new DownloadManager(s3FileName, localInputPath + filename, medicalImageBkt);
                    downloadExecutor.execute(downloadManager);
                }
            } else {
                logger.error("downloadFiles :: S3:Object does not exists:" + this.medicalImageBkt + SLASH + s3FileName);
                throw new FileNotFoundException();
            }
        }
        waitForExecutorCompletion(downloadExecutor);
        logger.info("Completed downloading");
    }

    /**
     * Method to download files in the given list
     *
     * @param summaries map of filenames and bucket
     * @param config    Config
     */
    public void downloadFilesFromList(Map<String, String> summaries, Config config) {
        logger.info("downloadFilesFromList :: Starting downloadFilesFromList");
        ExecutorService downloadExecutor = getExecutors(FOUR);
        for (String summary : summaries.keySet()) {
            String s3FileName = summary;

            String[] filePaths = s3FileName.split(SLASH);
            String filename = filePaths[filePaths.length - 1];
            if (filename.endsWith(PDF_EXT) || filename.toUpperCase().endsWith(TIF) || filename.toUpperCase()
                    .endsWith(TIFF)) {
                updatedFileNameList.add(filename.replaceFirst(TIFF_FILE_EXT_REGEX, PDF_EXT));
            }
            DownloadAndSplitManager downloadAndSplitManager =
                    new DownloadAndSplitManager(s3FileName, localInputPath + filename, summaries.get(summary),
                            jdbcConnection, config.getOutputBucket(), config.getOutputS3Path(), config);
            downloadExecutor.execute(downloadAndSplitManager);
        }
        waitForExecutorCompletion(downloadExecutor);
    }

    /* Call multiple threads to invoke pdf merge and upload to s3
     * - Grouping the files on year basis (claim_number-YEAR) and Sorting the file list in reverse chronological if
     * the list is not sorted  or date contained in the filename
     *
     * order to merger form latest to oldest - latest on top
     *
     * The filename format is claim_number-yyyymmdd-*.pdf
     *
     * - calling the PDFMergeManager runnable to merge the files
     * */
    public void mergePDFAndUploadToS3() {
        ExecutorService mergeExecutor = getExecutors(TEN);
        logger.info("Found " + s3Filenames.size() + " years worth of files");
        s3Filenames.keySet().forEach(pdfFileName -> {

            if (!this.isSorted) {
                logger.info("Sorting the file in order of date");
                s3Filenames.get(pdfFileName).sort(Collections.reverseOrder());
            }

            PDFMergeManager pdfMergeManager = new PDFMergeManager(localInputPath, localOutputPath, outputBucket,
                    s3OutputPath + pdfFileName + PDF_EXT, pdfFileName, s3Filenames.get(pdfFileName));
            mergeExecutor.execute(pdfMergeManager);
        });

        //Waiting for merge per year to get completed and Shutting down the downlaodExecutor
        waitForExecutorCompletion(mergeExecutor);
    }

    /* Initialize an executor service
     * */
    public ExecutorService getExecutors(int noOfThreads) {
        return Executors.newFixedThreadPool(noOfThreads);
    }

    /* Shutdown and wait for the completion of the ExecutorService
     * */
    public void waitForExecutorCompletion(ExecutorService executor) {
        executor.shutdown();
        while (!executor.isTerminated()) {

        }
    }
}
